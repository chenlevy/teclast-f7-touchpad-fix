#+title: Fix for the Teclast F7 touch-pad for Ubuntu
#+date: <2018-06-15 Fri>
#+author: Chen Levy
#+email: gitlab.com@chenlevy.com

*WARNING: This project should no longer be considered required/correct/useful*

As of Ubuntu 18.04.4 you only need to install the [[https://wiki.ubuntu.com/Kernel/LTSEnablementStack][Hardware Enablement Stack]] to use the /Teclast F7/'s touch pad.

* Introduction

Linux users that wish to use the /Teclast F7/ will find that its touch-pad isn't recognised by the Linux kernel out of the box.

While this is a work in progress, and I expect that in the future it will be a non-issue, as for this writing a GNU/Linux user need to jump through several hoops in order to make the touch-pad work for her.

This project tries to simplify the process in the meantime, using the patch created by [[https://github.com/brotfessor/sipodev][Brotfessor]] and creating an automatic procedure to reproduce  [[https://github.com/pokulan/Ubuntu-Kernel-4.15.0-20-for-Teclast-F7][Wojciech Zomkowski's]] and [[https://github.com/sridwan/Ubuntu-Kernel-for-Teclast-F7][sridwan's]] work following the [[https://wiki.ubuntu.com/Kernel/BuildYourOwnKernel][Build Your Own Kernel]] Ubuntu Wiki page and [[https://blobfolio.com/2018/06/building-a-custom-xanmod-kernel-on-ubuntu-18-04/][Building Custom XanMod Linux Kernel on Ubuntu 18.04 (Bionic Beaver) - Blobfolio]]

* Disclaimer

I am writing this project to scratch my own itch.  I take no responsibility for your system's integrity, the health of your cat or your hair line.

I never claimed that I know what I am doing, so the usual guarantee applies: /If it breaks you get to keep both halfs/.

* License

GPLv2

* Usage

Although it is a work in progress, I was able to build a working kernels with:

#+BEGIN_SRC sh
# Prepare the enviroment on your host (once per host)
sudo scripts/step.sh 0
# Build the kernel (once per kernel version)
scripts/step.sh 1 2 3 4 5 6
#+END_SRC

I built it on and for my /Teclast F7/ running /Kubuntu 18.04/, and while it *should* work on other *buntus YMMV.

* Patches

- [[https://patchwork.kernel.org/patch/10138399/b][pager]]
- [[https://marc.info/?l=linux-kernel&m=152116992412107&w=2][str_error_r]]
