#!/bin/bash -e

function usage() {
    cat <<EOF
Usage:
   $0 NUMBER ...

Where:
   NUMBER one of:
      0  -- install packages (as root)
      1  -- get sources
      2  -- ignore last version
      3  -- patch the kernel
      4  -- update changelog
      5  -- clean before build
      6  -- build the kernel
EOF
    exit 1
}

function assert_normal_user() {
    if [ $(id -u) -eq 0 ] ; then
        echo "This step shuldn't be run as root" 1>&2
        exit 1
    fi
}

function assert_root() {
    if [ $(id -u) -ne 0 ] ; then
        echo "This step needs root privilages" 2>&1
        echo "please run 'sudo $0'" 2>&1
        exit 1
    fi
}


function install_packages() {
    assert_root

    # udpate sources list
    sed -n 's/^#\s\(deb-src .*\)/\1/p' /etc/apt/sources.list > /etc/apt/sources.list.d/enabled_sources.list
    apt update

    apt install git build-essential kernel-package fakeroot ccache bison flex \
        libncurses5-dev libssl-dev libudev-dev libpci-dev libelf-dev

    ## gcc-8 is a nice to have - but duce to new warnings it fails to
    ## complie the Ubuntu kernel
    # apt install gcc-8 g++-8
    # date-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 800 --slave /usr/bin/g++ g++ /usr/bin/g++-8
    # date-alternatives --config gcc

    # TODO: need a better way to choose kerenl version
    apt build-dep linux-image-$(uname -r)
}


function get_sources() {
    assert_normal_user

    ## this disn't brint the intire kernel, just the meta data of the package
    # apt source linux-image-$(uname -r)

    ## the actual kerenl source
    mkdir -p ~/src
    cd ~/src
    if [ -d $SOURCE_DIR/.git/ ] ; then
        (
            cd $SOURCE_DIR
            git reset --hard HEAD
            git clean -fd
            git pull
        )
    else
        git clone git://kernel.ubuntu.com/ubuntu/ubuntu-${DISTRIB_CODENAME}.git
    fi

    if [ -d $PATCH_DIR/.git/ ] ; then
        (
            cd $PATCH_DIR
            git reset --hard HEAD
            git clean -fd
            git pull
        )
    else
        git clone git@github.com:brotfessor/sipodev.git
    fi
}

function ignore_last_version() {
    assert_normal_user

    local version=$(sed -n '1s/^.*(\([0-9][0-9.-]*\)).*$/\1/p' $CHANGELOG)
    local ignore_dir=$SOURCE_DIR/debian.master/abi/$version/$(dpkg --print-architecture)/
    mkdir -p $ignore_dir
    touch $ignore_dir/ignore{,.modules}
}

function patch_the_kernel() {
    assert_normal_user

    for p in $PROJ_DIR/patches/*.patch ; do
        patch --strip 1 --directory=$SOURCE_DIR < $p
    done

    ## option a
    # patch --strip=1 \
    #       --directory=$SOURCE_DIR/drivers/hid/i2c-hid/ \
    #       < $PATCH_DIR/properpatch/patch3.patch

    # option b
    cp $PATCH_DIR/b/i2c-hid.c $SOURCE_DIR/drivers/hid/i2c-hid/i2c-hid.c
}

function copy_patch_to_build() {
    for f in drivers/hid/i2c-hid/i2c-hid.c \
                 tools/lib/subcmd/pager.c \
                 tools/lib/str_error_r.c ; do
        if [ -d $(dirname $SOURCE_DIR/debian/build/tools-perarch/$f) ] ; then
            cp $SOURCE_DIR/$f $SOURCE_DIR/debian/build/tools-perarch/$f
        fi
    done
}

function update_changelog() {
    assert_normal_user

    # changelog=linux-signed-$(uname -r | cut -d"-" -f1)/debian/changelog
    local title=$(sed -ne '1s/\([0-9][0-9.-]*\)/\1+f7/p' $CHANGELOG)
    mv $CHANGELOG ${CHANGELOG}.orig
    cat > ${CHANGELOG} <<EOF
${title}

  * Fix Teclast F7 touchpad by applying brotfessor's sipodev patch

 -- Chen Rotem Levy <gitlab.com@chenlevy.com>  $(date +"%a, %d %b %Y %H:%M:%S %z")

EOF
    cat ${CHANGELOG}.orig >> ${CHANGELOG}
}

function clean_slate() {
    assert_normal_user

    (
        cd $SOURCE_DIR
        fakeroot debian/rules clean
    )
}

function build_the_kernel() {
    assert_normal_user

    (
        cd $SOURCE_DIR
        # export MAKEFLAGS="-j"  # try to make the build faster using all the cores
        export CFLAGS="-Wno-format-truncation"
        fakeroot debian/rules binary-headers binary-generic binary-perarch
    )
}

# MAIN

PROJ_DIR=$(cd $(dirname $0) && cd .. && pwd)

# get DISTIB_CODENAME
source /etc/lsb-release

## for cuture use
#INSTALLED_KERNEL_VERSIONS=$(dpkg --list linux-image* \
#                               | awk '$1=="ii" && match($3,/-/) {print $3}')

SOURCE_DIR=~/src/ubuntu-${DISTRIB_CODENAME}
PATCH_DIR=~/src/sipodev
CHANGELOG=$SOURCE_DIR/debian.master/changelog

[ -n "$1" ] || usage

for x ; do
    case $x in
        0) install_packages ;;
        1) get_sources ;;
        2) ignore_last_version ;;
        3) patch_the_kernel ;;
        3.5) copy_patch_to_build ;;
        4) update_changelog ;;
        5) clean_slate ;;
        6) build_the_kernel ;;
        *) usage ;;
    esac
done
